﻿
using Android.App;
using Android.Content.PM;
using Android.OS;

namespace MyBestHelper.Droid
{
    [Activity (Label = "MyBestHelper", Icon = "@drawable/mybesthelper_logo", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, ScreenOrientation = ScreenOrientation.Portrait)]
	public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsApplicationActivity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			global::Xamarin.Forms.Forms.Init (this, bundle);
            FFImageLoading.Forms.Droid.CachedImageRenderer.Init();
            LoadApplication (new MyBestHelper.App ());
		}
	}
}

