﻿using MyBestHelper.Business;
using MyBestHelper.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace MyBestHelper.Pages
{
    public class MainPage : CarouselPage
    {
        private static List<Question> _questions;
        public static List<Question> AvailableQuestions => _questions ?? (_questions = Assets.ListAvailableQuestions().OrderBy(q => q.Order).ToList());

        public MainPage()
        {
            Title = "My Best Helper";
            BackgroundColor = Color.FromHex("f3f3f3");
            Load();
        }

        private void Load()
        {
            CurrentPageChanged += (sender, e) =>
            {
                (CurrentPage as QuestionPage)?.UpdateParentTitle();
            };

            for (int i = 0; i < AvailableQuestions.Count; i++)
            {
                Children.Add(new QuestionPage(AvailableQuestions[i]));
            }
        }
    }
}
