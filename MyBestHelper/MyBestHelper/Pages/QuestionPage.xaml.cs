﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MyBestHelper.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class QuestionPage : ContentPage
    {
        public static readonly BindableProperty QuestionProperty = BindableProperty.Create<QuestionPage, string>(p => p.Question, string.Empty);

        public string Question
        {
            get
            {
                return (string)GetValue(QuestionProperty);
            }
            set
            {
                SetValue(QuestionProperty, value);
            }
        }

        public static readonly BindableProperty SrcImageOneProperty = BindableProperty.Create<QuestionPage, string>(p => p.SrcImageOne, string.Empty);

        public string SrcImageOne
        {
            get
            {
                return (string)GetValue(SrcImageOneProperty);
            }
            set
            {
                SetValue(SrcImageOneProperty, value);
            }
        }

        public static readonly BindableProperty SrcImageTwoProperty = BindableProperty.Create<QuestionPage, string>(p => p.SrcImageTwo, string.Empty);

        public string SrcImageTwo
        {
            get
            {
                return (string)GetValue(SrcImageTwoProperty);
            }
            set
            {
                SetValue(SrcImageTwoProperty, value);
            }
        }

        public static readonly BindableProperty SrcImageThreeProperty = BindableProperty.Create<QuestionPage, string>(p => p.SrcImageThree, string.Empty);

        public string SrcImageThree
        {
            get
            {
                return (string)GetValue(SrcImageThreeProperty);
            }
            set
            {
                SetValue(SrcImageThreeProperty, value);
            }
        }

        public static readonly BindableProperty SrcImageFourProperty = BindableProperty.Create<QuestionPage, string>(p => p.SrcImageFour, string.Empty);

        public string SrcImageFour
        {
            get
            {
                return (string)GetValue(SrcImageFourProperty);
            }
            set
            {
                SetValue(SrcImageFourProperty, value);
            }
        }

        protected override void OnPropertyChanged(string propertyName)
        {
            base.OnPropertyChanged(propertyName);
        }

        public QuestionPage()
        {
            InitializeComponent();
        }
    }
}
