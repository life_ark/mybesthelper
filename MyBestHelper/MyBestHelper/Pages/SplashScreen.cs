﻿using FFImageLoading.Forms;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MyBestHelper.Pages
{
    public class SplashScreen: ContentPage
    {
        public SplashScreen()
        {
            BackgroundColor = Color.FromHex("f3f3f3");

            var mainGrid = new Grid
            {
                VerticalOptions = LayoutOptions.CenterAndExpand,
                HorizontalOptions = LayoutOptions.CenterAndExpand                
            };

            mainGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            mainGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(8, GridUnitType.Star) });
            mainGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });

            var fullLogo = new CachedImage
            {
                Source = "full_logo.png",
                VerticalOptions = LayoutOptions.CenterAndExpand,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                DownsampleToViewSize = true                
            };

            mainGrid.Children.Add(fullLogo);
            Grid.SetColumn(fullLogo, 1);

            Content = mainGrid;            
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            NavigationPage.SetHasNavigationBar(this, false);
            await Task.Delay(1000);
            NavigationPage.SetHasNavigationBar(this, true);
            await Navigation.PushAsync(new MainPage());
            Navigation.RemovePage(this);
        }
    }
}
