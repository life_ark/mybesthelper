﻿using MyBestHelper.Business;
using MyBestHelper.Models;
using System.Collections.Generic;
using System.Linq;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MyBestHelper.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : CarouselPage
    {
        public MainPage()
        {
            InitializeComponent();
            Load();

        }

        private void Load()
        {
            List<Question> questions = Assets.ListAvailableQuestions();
            foreach (var question in questions.OrderBy(q => q.Order))
            {
                var questionPage = new QuestionPage
                {
                    Question = question.Text,
                    SrcImageOne = question.Answers[0].SrcImage,
                    SrcImageTwo = question.Answers[0].SrcImage,
                    SrcImageThree = question.Answers[0].SrcImage,
                    SrcImageFour = question.Answers[0].SrcImage
                };

                Children.Add(questionPage);
            }
        }
    }
}
