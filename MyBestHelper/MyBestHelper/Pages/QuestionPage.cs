﻿using System;
using FFImageLoading.Forms;
using Xamarin.Forms;
using MyBestHelper.Components;
using MyBestHelper.Models;
using System.Collections.Generic;
using MyBestHelper.Business;

namespace MyBestHelper.Pages
{
    public class QuestionPage : ContentPage
    {
        public Question Question { get; set; }

        public bool ShowFinishButton
        {
            get
            {
                return MainPage.AvailableQuestions.Count == Question.Order;
            }
        }

        public QuestionPage(Question question)
        {
            Question = question;
            Load(question);
        }

        private void Load(Question question)
        {
            Grid mainGrid = new Grid
            {
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                RowSpacing = 0,
                Padding = new Thickness(5, 0, 5, 5)
            };

            if (!ShowFinishButton)
            {
                mainGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
                mainGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(9, GridUnitType.Star) });
            }
            else
            {
                mainGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
                mainGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(8, GridUnitType.Star) });
                mainGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            }

            var stackTopLabel = new StackLayout
            {
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand
            };

            stackTopLabel.Children.Add(new Label
            {
                Text = question.Text,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
                TextColor = Color.FromHex("3e4956")

            });

            stackTopLabel.Children.Add(new Label
            {
                Text = "(when you are done, swipe left or right)",
                VerticalOptions = LayoutOptions.CenterAndExpand,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                TextColor = Color.FromHex("3e4956")

            });

            mainGrid.Children.Add(stackTopLabel);
            Grid.SetRow(stackTopLabel, 0);
            Grid.SetColumnSpan(stackTopLabel, 2);

            Grid answersGrid = new Grid
            {
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                RowSpacing = 0,
                ColumnSpacing = 0
            };
            answersGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            answersGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            answersGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            answersGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });

            List<Color> colors = AnswerView.GetColors();

            answersGrid.Children.Add(new AnswerView(question.Answers[0], colors[0]), 0, 0);
            answersGrid.Children.Add(new AnswerView(question.Answers[1], colors[1]), 0, 1);
            answersGrid.Children.Add(new AnswerView(question.Answers[2], colors[2]), 1, 0);
            answersGrid.Children.Add(new AnswerView(question.Answers[3], colors[3]), 1, 1);
            mainGrid.Children.Add(answersGrid);
            Grid.SetRow(answersGrid, 1);
            Grid.SetColumnSpan(answersGrid, 2);


            if (ShowFinishButton)
            {
                Button btnResults = new Button
                {
                    Text = "Show my Results!",
                    VerticalOptions = LayoutOptions.Center,
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    BackgroundColor = Color.FromHex("ef5757"),
                    FontFamily = "Proxima Nova",
                    FontAttributes = FontAttributes.Bold
                };

                btnResults.Clicked += async (sender, e) =>
                {
                    await Navigation.PushAsync(new ResultPage(Assets.GetRandomResult()));
                };

                var stackButton = new StackLayout
                {
                    VerticalOptions = LayoutOptions.CenterAndExpand,
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                };

                stackButton.Children.Add(btnResults);

                mainGrid.Children.Add(stackButton);
                Grid.SetRow(stackButton, 2);
                Grid.SetColumnSpan(stackButton, 2);
            }

            Content = mainGrid;
        }

        public void UpdateParentTitle()
        {
            (Parent as MainPage).Title = $"Question {Question.Order} from {MainPage.AvailableQuestions.Count}";
        }
    }
}