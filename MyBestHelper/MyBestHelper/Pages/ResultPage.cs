﻿using System;
using MyBestHelper.Models;
using Xamarin.Forms;
using FFImageLoading.Forms;

namespace MyBestHelper.Pages
{
    public class ResultPage : ContentPage
    {
        public ResultPage(Result result)
        {
            Title = "You are the....";
            BackgroundColor = BackgroundColor = Color.FromHex("f3f3f3");
            Load(result);
        }

        private void Load(Result result)
        {
            Grid mainGrid = new Grid
            {
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand,
                Padding = new Thickness(0.5, 0.5, 0.5, 0.5)
            };

            mainGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            mainGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(5, GridUnitType.Star) });
            mainGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(4, GridUnitType.Star) });

            var labelCool = new Label
            {
                VerticalOptions = LayoutOptions.CenterAndExpand,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
                TextColor = Color.FromHex("929497"),
                FontFamily = "Noto Sans",
                Text = "Cool you are the..."
            };

            mainGrid.Children.Add(labelCool);
            Grid.SetRow(labelCool, 0);
            Grid.SetColumnSpan(labelCool, 2);

            var cachedImage = new CachedImage
            {
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Source = result.Image
            };

            mainGrid.Children.Add(cachedImage);
            Grid.SetRow(cachedImage, 1);
            Grid.SetColumnSpan(cachedImage, 2);

            var stackDescription = new StackLayout
            {
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Padding = new Thickness(1, 1, 1, 1)
            };

            stackDescription.Children.Add(new Label
            {
                VerticalOptions = LayoutOptions.Start,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)),
                TextColor = Color.FromHex("3e4956"),
                FontAttributes = FontAttributes.Bold,
                Text = result.Title
            });

            stackDescription.Children.Add(new Label
            {
                VerticalOptions = LayoutOptions.StartAndExpand,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                XAlign = TextAlignment.Center,
                FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
                TextColor = Color.FromHex("929497"),
                Text = result.Description,
                FontFamily = "Noto Sans",
            });

            mainGrid.Children.Add(stackDescription);
            Grid.SetRow(stackDescription, 2);
            Grid.SetColumnSpan(stackDescription, 2);

            Content = mainGrid;
        }
    }
}
