﻿namespace MyBestHelper.Models
{
    public class Result
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
    }
}
