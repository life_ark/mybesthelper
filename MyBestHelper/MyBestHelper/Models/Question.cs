﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyBestHelper.Models
{
    public class Question
    {
        public int Order { get; set; }
        public string Text { get; set; }
        public List<Answer> Answers { get; set; }
    }
}
