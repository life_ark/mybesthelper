﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyBestHelper.Models
{
    public class Answer
    {
        public int Order { get; set; }
        public string Text { get; set; }
        public string BackgroundImage { get; set; }
        public string IconImage { get; set; }
    }
}
