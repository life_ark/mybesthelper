﻿using FFImageLoading.Forms;
using MyBestHelper.Models;
using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace MyBestHelper.Components
{
    public class AnswerView : ContentView
    {
        private bool _tapped = false;

        public static Color Blue => Color.FromRgb(71, 148, 255);
        public static Color Red => Color.FromRgb(239, 87, 87);
        public static Color Gray => Color.FromRgb(197, 197, 197);
        public static Color Yellow => Color.FromRgb(241, 190, 82);

        private Color _selectedColor;
        public Color SelectedColor => _selectedColor;

        public AnswerView(Answer answer, Color color)
        {
            _selectedColor = color;

            RelativeLayout layout = new RelativeLayout();

            var backgroundImage = new CachedImage()
            {
                Source = answer.BackgroundImage,
                Aspect = Aspect.AspectFill
            };

            layout.Children.Add(backgroundImage,
                Constraint.Constant(0),
                Constraint.Constant(0),
                Constraint.RelativeToParent((parent) =>
                {
                    return parent.Width;
                }),
                Constraint.RelativeToParent((parent) =>
                {
                    return parent.Height;
                }));

            var stackColorChange = new StackLayout
            {
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                BackgroundColor = SelectedColor
            };

            layout.Children.Add(stackColorChange,
                Constraint.Constant(0),
                Constraint.Constant(0),
                Constraint.RelativeToParent((parent) =>
                {
                    return parent.Width;
                }),
                Constraint.RelativeToParent((parent) =>
                {
                    return parent.Height;
                }));

            var iconImage = new CachedImage() { Source = answer.IconImage };

            layout.Children.Add(
                iconImage,
                Constraint.RelativeToParent((parent) =>
                {
                    return ((parent.Width / 2) - (iconImage.Width / 2));
                }),
                Constraint.RelativeToParent((parent) =>
                {
                    return parent.Height * .25;
                }),
                Constraint.RelativeToParent((parent) =>
                {
                    return parent.Width * .45;
                }),
                Constraint.RelativeToParent((parent) =>
                {
                    return parent.Width * .45;
                })
            );

            iconImage.SizeChanged += (sender, e) =>
            {
                layout.ForceLayout();
            };

            var dashlabel = new Label()
            {
                Text = answer.Text,
                XAlign = TextAlignment.Center,
                TextColor = Color.White,
                FontFamily = "Noto Sans"
            };

            layout.Children.Add(dashlabel,
                Constraint.Constant(0),
                Constraint.RelativeToParent((parent) =>
                {
                    return parent.Height - 30;
                }),
                Constraint.RelativeToParent((parent) =>
                {
                    return parent.Width;
                }),
                Constraint.RelativeToParent((parent) =>
                {
                    return parent.Height;
                }));

            var tap = new TapGestureRecognizer();
            tap.Tapped += (sender, e) =>
            {
                _tapped = !_tapped;

                if (_tapped)
                {
                    stackColorChange.BackgroundColor = Color.Transparent;
                }
                else
                {
                    stackColorChange.BackgroundColor = SelectedColor;
                }
            };


            GestureRecognizers.Add(tap);
            stackColorChange.GestureRecognizers.Add(tap);
            Content = layout;
        }

        public static List<Color> GetColors()
        {
            List<Color> colors = new List<Color>()
            {
                Red,Gray,Blue,Yellow
            };

            Shuffle(colors);
            return colors;
        }

        private static Random rng = new Random();
        private static void Shuffle<T>(IList<T> list)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }
    }
}
