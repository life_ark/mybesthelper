﻿using MyBestHelper.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace MyBestHelper.Business
{
    public class Assets
    {
        private static string jsonQuestions = @"[{'Order':1,'Text':'What work are you most interested in?','Answers':[{'Order':1,'Text':'Child care','BackgroundImage':'answer_child_care.jpg'},{'Order':2,'Text':'Elder care','BackgroundImage':'answer_elder_care.png'},{'Order':3,'Text':'Home care','BackgroundImage':'answer_home_care.jpg'},{'Order':4,'Text':'Pet care','BackgroundImage':'answer_pet_care.jpg'}]},{'Order':2,'Text':'Do you want to work','Answers':[{'Order':1,'Text':'Full-time','BackgroundImage':'answer_full_time_job.jpg'},{'Order':2,'Text':'Part-time','BackgroundImage':'answer_part_time_job.jpg'},{'Order':3,'Text':'Occasional','BackgroundImage':'answer_ocasionally_job.jpg'},{'Order':4,'Text':'On-call','BackgroundImage':'answer_oncall_job.jpg'}]},{'Order':3,'Text':'Which person are you most like?','Answers':[{'Order':1,'Text':'Gets things done','BackgroundImage':'answer_getthinsdone_people.jpg'},{'Order':2,'Text':'Always happy','BackgroundImage':'answer_happy_people.jpg'},{'Order':3,'Text':'Friendly and fun','BackgroundImage':'answer_friendly_an_fun_people.jpg'},{'Order':4,'Text':'Professional','BackgroundImage':'answer_professional_people.jpg'}]},{'Order':4,'Text':'Your favorite music is:','Answers':[{'Order':1,'Text':'Modern pop','BackgroundImage':'answer_modern_pop_music.jpg'},{'Order':2,'Text':'Rock and roll','BackgroundImage':'answer_rock_music.jpg'},{'Order':3,'Text':'Classic','BackgroundImage':'answer_classic_music.jpg'},{'Order':4,'Text':'World music','BackgroundImage':'answer_world_music.jpg'}]},{'Order':5,'Text':'I would be most happy','Answers':[{'Order':1,'Text':'Cooking a meal','BackgroundImage':'answer_cooking_meal.jpg'},{'Order':2,'Text':'Taking a walk','BackgroundImage':'answer_taking_a_walk.jpg'},{'Order':3,'Text':'Arts and Crafts','BackgroundImage':'answer_arts_and_crafts.jpg'},{'Order':4,'Text':'Playing sports','BackgroundImage':'answer_playing_sports.jpg'}]},{'Order':6,'Text':'The coolest thing in this list is:','Answers':[{'Order':1,'Text':'Gardening','BackgroundImage':'answer_gardening.jpg'},{'Order':2,'Text':'Play an instrument','BackgroundImage':'answer_playing_instrument.jpg'},{'Order':3,'Text':'Travelling','BackgroundImage':'answer_travelling.jpg'},{'Order':4,'Text':'Seeing a show','BackgroundImage':'answer_rock_music.jpg'}]},{'Order':7,'Text':'I prefer spending time with people who are:','Answers':[{'Order':1,'Text':'Easy going','BackgroundImage':'answer_easy_going.jpg'},{'Order':2,'Text':'Super smart','BackgroundImage':'answer_super_smart.jpg'},{'Order':3,'Text':'Open and direct','BackgroundImage':'answer_open_and_direct.jpg'},{'Order':4,'Text':'Laugh a lot','BackgroundImage':'answer_happy_people.jpg'}]},{'Order':8,'Text':'I am great at','Answers':[{'Order':1,'Text':'Listening to instructions','BackgroundImage':'answer_instructions.png'},{'Order':2,'Text':'Problem solving','BackgroundImage':'answer_problem_solving.jpg'},{'Order':3,'Text':'Using my experience','BackgroundImage':'answer_experience.jpg'},{'Order':4,'Text':'Learning from others','BackgroundImage':'answer_learning_others.jpg'}]},{'Order':9,'Text':'I relax by','Answers':[{'Order':1,'Text':'Reading books','BackgroundImage':'answer_reading_books.jpg'},{'Order':2,'Text':'Doing something active','BackgroundImage':'answer_doing_something_active.jpg'},{'Order':3,'Text':'Hanging out with friends','BackgroundImage':'answer_friendly_an_fun_people.jpg'},{'Order':4,'Text':'Watching movies','BackgroundImage':'answer_movies.jpg'}]},{'Order':10,'Text':'When I choose presents for friend','Answers':[{'Order':1,'Text':'Easy - I have lots of ideas','BackgroundImage':'answer_lots_gifts.jpg'},{'Order':2,'Text':'I plan long ahead','BackgroundImage':'answer_plan_gift.jpg'},{'Order':3,'Text':'I am never sure','BackgroundImage':'answer_gift_doubt.jpg'},{'Order':4,'Text':'I am totally last minute!','BackgroundImage':'answer_last_minute.jpg'}]}]";
        private static string jsonResuls = @"[{'Image':'answer_happiest_helper.jpg','Title':'Happiest Helper','Description':'I bring joy and fun into every situation. I love people and am thrilled to know I can make their lives better.'},{'Image':'answer_get_things_done.jpg','Title':'Get things done','Description':'Energetic multitasker, I am the person everyone looks to when they want to make sure all is handled! '},{'Image':'answer_reliable.jpg','Title':'Most reliable','Description':'All friends  always turn to me as I am like a rock - I will always be there for them and help them out.'},{'Image':'answer_happy_einstein.jpg','Title':'Like a genie','Description':'I just know what needs to be done, and do it! No instructions required, when I am in charge, everything is better.'}]";

        public static List<Question> ListAvailableQuestions()
        {
            var questions = JsonConvert.DeserializeObject<List<Question>>(jsonQuestions);
            return questions;
        }

        public static List<Result> ListAvailableResults()
        {
            var results = JsonConvert.DeserializeObject<List<Result>>(jsonResuls);
            return results;
        }

        public static Result GetRandomResult()
        {
            List<Result> results = ListAvailableResults();
            Random r = new Random();
            return results[r.Next(results.Count)];
        }
    }
}
