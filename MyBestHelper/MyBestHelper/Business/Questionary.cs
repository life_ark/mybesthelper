﻿using System;
using System.Collections.Generic;
using System.Text;
using MyBestHelper.Models;
using System.Linq;

namespace MyBestHelper.Business
{
    public class Questionary
    {
        private static List<Question> _questions;
        private static int _lastPos = 0;
        public static int LastPos => _lastPos;
        public static List<Question> AvailableQuestions => _questions ?? (_questions = Assets.ListAvailableQuestions().OrderBy(q => q.Order).ToList());


        public static bool Beginning { get; set; }
        public static bool End { get; set; }

        public static Question Get(int pos = 0)
        {
            if (pos < 0 || pos > (AvailableQuestions.Count - 1))
                return null;

            _lastPos = pos;

            return AvailableQuestions[_lastPos];
        }
    }
}
